package az.rest.service;

import az.rest.exception.DataNotFoundException;
import az.rest.mapper.StudentMapper;
import az.rest.model.dto.StudentDto;
import az.rest.model.request.StudentRequest;
import az.rest.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class StudentService {
    private static final StudentMapper mapper = StudentMapper.INSTANCE;
    private final StudentRepository studentRepository;

    public List<StudentDto> getStudents() {
        return mapper.toStudentDtoList(studentRepository.findAll());
    }

    public StudentDto getStudentById(Long id) {
        var student = studentRepository.findById(id).orElseThrow(DataNotFoundException::studentNotFount);
        return mapper.toStudentDto(student);
    }

    public StudentDto createStudent(StudentRequest studentRequest) {
        var student = studentRepository.save(mapper.toStudent(studentRequest));
        return mapper.toStudentDto(student);
    }

    public StudentDto updateStudent(Long id, StudentRequest studentRequest) {
        studentRepository.findById(id).orElseThrow(DataNotFoundException::studentNotFount);
        var student = mapper.toStudent(id, studentRequest);
        studentRepository.save(student);
        return mapper.toStudentDto(student);
    }

    public void deleteStudent(Long id) {
        studentRepository.findById(id).orElseThrow(DataNotFoundException::studentNotFount);
        studentRepository.deleteById(id);
    }
}
