package az.rest.model.request;

import lombok.Data;

import java.time.LocalDate;

@Data
public class StudentRequest {
    private String name;
    private String institute;
    private LocalDate birthDate;
}
