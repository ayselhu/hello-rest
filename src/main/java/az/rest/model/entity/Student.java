package az.rest.model.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
@Data
@Entity
@Table(name="student")
public class Student{
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String institute;
    private LocalDate birthDate;
}
