package az.rest.mapper;

import az.rest.model.dto.StudentDto;
import az.rest.model.entity.Student;
import az.rest.model.request.StudentRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StudentMapper {
    StudentMapper INSTANCE = Mappers.getMapper(StudentMapper.class);

    StudentDto toStudentDto(Student student);

    List<StudentDto> toStudentDtoList(List<Student> studentList);

    Student toStudent(StudentRequest studentRequest);

    @Mappings({@Mapping(target = "id", source = "id"),
            @Mapping(target = "name", source = "studentRequest.name"),
            @Mapping(target = "institute", source = "studentRequest.institute"),
            @Mapping(target = "birthDate", source = "studentRequest.birthDate")})
    Student toStudent(Long id, StudentRequest studentRequest);

}
