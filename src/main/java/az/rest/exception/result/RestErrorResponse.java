package az.rest.exception.result;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class RestErrorResponse {
    private String code;
    private String message;

}
