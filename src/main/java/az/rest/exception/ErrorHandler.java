package az.rest.exception;

import az.rest.exception.result.RestErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(BaseException.class)
    public ResponseEntity handleException(BaseException ex) {
        return new ResponseEntity<>(
                new RestErrorResponse(ex.getErrorCode(),ex.getErrorMessage()),
                ex.getHttpStatus());
    }
}
