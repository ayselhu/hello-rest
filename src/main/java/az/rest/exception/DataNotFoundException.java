package az.rest.exception;

import org.springframework.http.HttpStatus;

public class DataNotFoundException extends BaseException {

    public DataNotFoundException(String errorMessage) {
        super(HttpStatus.NOT_FOUND, "DATA_NOT_FOUND", errorMessage);
    }

    public static DataNotFoundException studentNotFount() {
        return new DataNotFoundException("student not found");
    }
}
